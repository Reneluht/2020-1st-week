﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tsüklid
{
    class Program
    {
        static void Main(string[] args)
        {
            //if (true) // shortcut Ctrl + K + S, auto bloki vorminus, ESIMENE TSÜKKEL
            //{
            //    Console.WriteLine();
            //    int n = 10;
            //    int[] arvud = new int[n]; 
            //}
            //int[] arvud = new int[8];
            //for (int i = 1; i < arvud.Length; i++) // ESIMENE TSÜKKEL
            //{
            //    Console.WriteLine($"Teen seda asja {i}. korda");
            //    arvud[i] = i * i;
            //}
            //foreach (var x in arvud) // TEINE TSÜKKEL
            //{
            //    Console.WriteLine($"\t{x}");
            //}
            //Console.WriteLine();

            //while(bool - avaldis)  // KOLMAS TSÜKKEL, korratakse seni kuni avaldis on true
            //{

            //}

            //do // NELJAS AVALDIS
            //{

            //} while (bool-avaldis) ; // korratakse seniu kuni avaldis on true

            //int[,] tabel = new int[10, 10];
            //for (int i = 0; i < tabel.GetLength(0); i++)
            //    for (int j = 0; j < tabel.GetLength(1); j++)
            //{
            //    tabel[i, j] = i * j;
            //    }
            ////foreach (var x in tabel) Console.WriteLine($" {x}");
            //for (int n = 0; n < 100; n++)
            //{
            //    Console.WriteLine($"{tabel[n / 10, n % 10]:00} \t {(n%10 == 9 ? "\n":"")}");
            //}
            //int[][] teinetabel = new int[10][];
            //for (int i = 0; i < teinetabel.Length; i++)
            //{
            //    teinetabel[i] = new int[10];
            //    for (int j = 0; j < teinetabel[i].Length; j++)
            //    {
            //        teinetabel[i][j] = i * j;
            //    }
            //}
            //foreach (var r in teinetabel)
            //{
            //    foreach (var x in r) Console.WriteLine($"{x:00} ");
            //    Console.WriteLine();
            //}
        }
    }
}
