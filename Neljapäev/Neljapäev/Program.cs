﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Neljapäev
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] arvudeMassiiv = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //List<int> arvudeList = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //int[] teine = new int[10];
            //List<int> teineList = new List<int>();

            //teineList.Add(17);

            //arvudeList.Add(23);
            //arvudeList.Remove(5);

            //arvudeMassiiv[3]++;
            //arvudeList[7]++;

            ////foreach (var x in arvudeMassiiv) Console.Write(x);
            //Console.WriteLine(String.Join(", ", arvudeMassiiv));
            //Console.WriteLine(String.Join(", ", arvudeList));

            ////foreach (var x in arvudeList) Console.Write(x);

            //string tekst = "Rene on tore mies";
            //var sonad = tekst.Split(' ');
            //foreach (var x in sonad) Console.WriteLine(x);

            //

            string failiNimi = "spordipäeva protokoll.txt";
            string[] loetudRead = File.ReadAllLines(failiNimi);

            foreach (var x in loetudRead) Console.WriteLine(x);

            string[] nimed = new string[loetudRead.Length];
            int[] ajad = new int[loetudRead.Length];
            int[] distantsid = new int[loetudRead.Length];
            double[] kiirused = new double[loetudRead.Length];

            for (int i = 1; i < loetudRead.Length; i++)
            {
                var reaOsad = loetudRead[i].Split(',');
                string nimi = reaOsad[0].Trim(); nimed[i] = nimi;
                int distants = int.Parse(reaOsad[1].Trim()); distantsid[i] = distants;
                int aeg = int.Parse(reaOsad[2].Trim()); ajad[i] = aeg;
                double kiirus = distants * 1.0 / aeg; kiirused[i] = kiirus;
                Console.WriteLine($"{nimi} jooksis {distants} meetrit ajaga {aeg}");

            }

            double maxKiirus = kiirused.Max();
            Console.WriteLine("\nKõige kiirem(ad) jooksja(d):\n");

            for (int i = 1; i < kiirused.Length; i++)
                if (kiirused[i] == maxKiirus)
                    Console.WriteLine($"{nimed[i]} jooksis {distantsid[i]}m kiirusega {kiirused[i]:F2}");

        }
    }
}
