﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedesedÜlesanded
{
    class Program
    {
        static void Main(string[] args)
        {
            const int KÜSIMUSI = 10;
            const int KATSEID = 5;

            int punkte = KÜSIMUSI * KATSEID; // siin hakkan punkte maha arvama
            Random r = new Random();

            for (int i = 0; i < KÜSIMUSI; i++) // kästi 10 korda
            {
                int y = r.Next(100); // kaks arvu
                int t = r.Next(100);
                int tehe = r.Next(4); // neli tehet
                (string teheSõna, int tulemus) =
                    tehe == 0 ? ("summa", y + t) :
                    tehe == 1 ? ("korrutis", y * t) :
                    tehe == 2 ? ("vahe", y - t) :
                    tehe == 3 ? ("jagatis", y / t) : ("", 0);
                Console.Write($"mis on {y} ja {t} {teheSõna}: ");
                for (int j = 0; j < KATSEID;)
                {
                    if (int.TryParse(Console.ReadLine(), out int vastus))
                    {
                        if (vastus == tulemus) break;
                        punkte--;
                        if (++j < KATSEID) Console.Write("pisut läks mööda, proovi uuesti: ");
                    }
                    else Console.WriteLine("vasta korrallikult - arv peab olema");
                }



            }
            Console.WriteLine(
                    punkte > 47 ? $"tubli said {punkte} punkti hinne 5" :
                    punkte > 37 ? $"hea said {punkte} punkti hinne 4" :
                    punkte > 27 ? $"kehvake said vaid {punkte} punkti hinne 3" :
                    punkte > 17 ? $"böö said ainult {punkte} punkti hinne 2" :
                    punkte > 10 ? $"äpu sa said vaid {punkte} punkti hinne 1" :
                    $"sinu {punkte} punktiga ei ole midagi peale hakata"
                    );
        }
    }
}
