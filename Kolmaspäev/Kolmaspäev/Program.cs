﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Teisendamine
{
    class Program
    {
        static void Main(string[] args)
        {
            ////TEISENDAMINE

            //int lyhike = 1000000;
            //Console.WriteLine($"muutuja lyh väärtus on {lyhike}");

            ////1.1 CASTING

            //long pikk = lyhike;
            //long teine = (long) (pikk + 7); // (long) tüüpi teisendust nimetatakse casting´uks
            //Console.WriteLine($"muutuja teine väärtus on {teine}");

            //// 1.2 STRING
            //// ToSting()
            //// TypeName.Parse(string) - teisendus vastavasse tüüpi
            //// TryParse()

            //string s = (4 + 7 * teine).ToString(); //
            //// sting.Format kasutab placeholderite kohal To.String'i
            //s = string.Format("see asi on {0}", 4 + 7 * teine); //võrdeline rida 26, {0} - placeholder
            //Console.WriteLine(s);
            //Console.WriteLine("see asi on {0}", 4 + 7 * teine);
            //Console.WriteLine($"see asi on {0}", 4 + 7 * teine);
            //// WriteLine kasutab string.Format
            //// string.FOrmat kasutab ToString
            //// $-sting avaldis kasutab sisemiselt string.Format

            //double d = Math.PI;
            //Console.WriteLine(d);

            //// kursoriga käsu peale + F1 - help browser, F12 - help VS
            //Console.WriteLine("täna on {0:dd.MMMM.yyyy}", DateTime.Today);
            //string täna = DateTime.Today.ToString("dd.MMMM.yyyy"); new CultureInfo("fi-fi");
            //Console.WriteLine(täna);

            //// suvaline andmetüüp (arv, kuupäev, kellaaeg) ToSting() - teisendab stringiks

            ////// vastupidi
            //// Console.WriteLine("mis päeval sa sündinud oled: ");
            //// DateTime sünnipäev = DateTime.Parse(Console.ReadLine());
            //// Console.WriteLine($"Sa oled siis {(DateTime.Today - sünnipäev).TotalDays} päeva vana");

            ////int.Parse(""); // teisendab teksti arvuks
            ////double testDouble = double.Parse("47.89");
            ////Console.WriteLine(testDouble);

            //if (DateTime.TryParse(Console.ReadLine(), out DateTime sünnipäev))
            //{
            //    Console.WriteLine(sünnipäev);
            //}
            //else
            //{
            //    Console.WriteLine("see pole miski kuupäev");
            //}

            //// 1.3 CONVERT
            //Console.WriteLine("anna üks arv: ");
            //int arv = Convert.ToInt32(Console.ReadLine());

            // Iseseisev ülesanne

            Console.WriteLine("mis on su sünnikuupäev: ");
            DateTime sünnipäev = DateTime.Parse(Console.ReadLine());
            Console.WriteLine($"Sa oled siis {(DateTime.Today - sünnipäev).Days / 365} aastat vana");

            Console.WriteLine("aga kui palju saad sa palka: ");
            decimal palk = decimal.Parse(Console.ReadLine());
            if (palk < 500)
            {
                Console.WriteLine($"Sinu palk on tulumaksuvaba");
            }
            else
            {
                Console.WriteLine($"sinu tulumaks on { (palk - 500) * 0.2M } eurot");
            }
            
             
        }
    }
}
