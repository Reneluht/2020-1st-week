﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NimeTeisendamine
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("anna üks nimi: ");
                string nimi = Console.ReadLine();
                if (nimi == "") break;
                // teisendus - nimega vaja teha midagi
                // 0. kõigepealt splitime
                var osad = nimi
                    .Replace("-", "- ")
                    .Split(' ');
                // iga osaga? Teeme tsükli
                for (int i = 0; i < osad.Length; i++)
                {
                    osad[i] = osad[i].Substring(0, 1).ToUpper() + osad[i].Substring(1).ToLower();  // 1. täht suureks ja ülejäänud väikeseks
                }
                nimi = string.Join(" ", osad).Replace("- ", "-"); // 0. pärast kokku tagasi
                Console.WriteLine(nimi);

                //Console.WriteLine(nimi.ToProper());

            }
        }
    }
}   
