﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardiÜL
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            var enne = Enumerable.Range(0, 52).ToArray();

            for (int i = 0; i < 10000; i++)
            {
                int x = r.Next(52);
                int y = r.Next(52);
                int t = enne[x];
                enne[x] = enne[y];
                enne[y] = t;
            }

            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{enne[i]:00}" + (i % 4 == 3 ? "\n" : "\t"));

                {

                }

            }
        }
    }
}
