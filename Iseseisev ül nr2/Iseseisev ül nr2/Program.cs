﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iseseisev_ül_nr2
{
    class Program
    {
        static void Main(String[] args)
        {
            DateTime[] sünnipäev =
            {
                new DateTime(1991, 10, 29),
                new DateTime(1992, 02, 27),
                new DateTime(1994, 06, 16),
                new DateTime(2004, 04, 03),
                new DateTime(1991, 08, 20),
                new DateTime(1992, 06, 06),
                new DateTime(1991, 11, 10),
                new DateTime(1971, 03, 18),
            };
            String[] nimed =
             {
                    "Rene",
                    "Merje",
                    "Marion",
                    "Margit",
                    "Martin",
                    "Roland",
                    "Raimo",
             };
            int vanusteSumma = 0;
            Console.WriteLine($"Meil on {nimed.Length} inimest");
            for (int i = 0; i < nimed.Length; i++)
            {
                Console.WriteLine($"{nimed[i]} kes on sündinud {sünnipäev[i]:dd.MMMM.yyyy}");
                vanusteSumma += (DateTime.Today - sünnipäev[i]).Days * 4 / 1461;
            }
            Console.WriteLine($"Keskmine vanus on {(vanusteSumma * 1.0 / nimed.Length) :F2}");

            //kuidas leida kõige hilisem kuupäev massiivist
            DateTime hiliseim = sünnipäev[0];
            int kelleoma = 0;
            for (int i = 1; i < sünnipäev.Length; i++)
            {
                if (sünnipäev[i] > hiliseim)
                {
                    hiliseim = sünnipäev[i];
                    kelleoma = i;
                }
            }
            Console.WriteLine($"Kõige noorem on {nimed[kelleoma]}");
            Console.WriteLine($"ta on {(DateTime.Today - sünnipäev[kelleoma]).Days * 4 / 1461} aastane");

            int kogum = 100;
            int vahemik = 100;

            int[] juhuslikud = new int[kogum];
            Random r = new Random();

            int suurim = -1;
            int väikseim = vahemik;
            int suurimKandidaat = 0;
            int väikseimKandidaat = 0;
            int summa = 0;
            int[] mitumida = new int[vahemik];

            for (int i = 0; i < juhuslikud.Length; i++)
            {
                int juhus;

                juhuslikud[i] = juhus = r.Next(vahemik);
                mitumida[juhus]++;
                Console.Write($"{juhus:00}{(i % 10 == 9 ? "\n" : " ")}");
                if (juhus > juhuslikud[suurimKandidaat])
                {
                    suurimKandidaat = i; suurim = juhus;
                }
                if (juhus < juhuslikud[väikseimKandidaat])
                {
                    väikseimKandidaat = i; väikseim = juhus;
                }
                summa += juhus;
            }
            Console.WriteLine($"suurim on {suurim} väikseim on {väikseim} keskmine on {summa * 1.0 / juhuslikud.Length}");
            Console.WriteLine($"suurim on {juhuslikud.Max()}");
            Console.WriteLine($"Keskmine on {juhuslikud.Average()}");

            string pole = "";
            string palju = "";
            int enim = mitumida.Max();
            for (int i = 0; i < mitumida.Length; i++)
            {
                if (mitumida[i] == 0) pole += $"{i:00} ";
                if (mitumida[i] == enim) palju += $"{i:00} ";
            }

            if (pole == "")
            {
                Console.WriteLine("Tabelis on kõiki arve");
            }
            else
            {
                Console.WriteLine($"Tabelis ei ole mitte ühtegi {pole}");
            }
            Console.WriteLine($"enim on {palju} - neid on {enim} tükki");

        }


    }


}