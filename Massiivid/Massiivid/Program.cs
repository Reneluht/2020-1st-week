﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {
            // MASSIIVID

            int[] arvud = new int[10]; // massiiv ehk array

            arvud[3] = 77; // loob arrays neljandale elemendile väärtuse
            arvud[1] = 100;
            int[] teine = arvud;
            arvud = new int[10]; // loob uue array
            teine[3]++;
            Console.WriteLine(teine[3] + arvud[1]);
            // võimalused kuidas märgistada array'd
            int[] teised = new int[] { 1, 2, 7, 3, 6, 8, 11 };
            int[] kolmas = { 1, 2, 7, 3, 6, 8, 11 };

            int[] yhestkymneni = Enumerable.Range(1, 10).ToArray(); // Järjestab arvud ühest kümneni
            Console.WriteLine(string.Join(", ", yhestkymneni));

            object[] eritüüp = new object[4];
            eritüüp[1] = "Rene";
            eritüüp[2] = "Merje";
            eritüüp[0] = "Tere";
            eritüüp[3] = 2020;
            Console.WriteLine(string.Join(", ", eritüüp));

            int[][] mison = new int[3][];
            mison[0] = new int[] { 1, 2, 3 };
            mison[1] = new int[] { 4, 5, };
            mison[2] = new int[] { 7, 8, 9 };
            int[][] seeon = { new int[] { 1, 2, 3 }, new int[] { 4, 5, }, new int[] { 7, 8, 9 } };

            // int[][] ei ole kahemõõtmeline massiiv
            // see on massiiv, mis koosneb massiividest

            int[,] tabel = new int[8, 8]; // see on 2 mõõtmeline massiiv
            int[,] tabel2 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9, } };

        }
    }
}
